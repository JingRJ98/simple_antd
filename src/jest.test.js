test('test common matcher', () => {
  expect(2 + 2).toBe(4)
  expect(2 + 2).not.toBe(5)
})

test('test to be true or false', () => {
  expect(1).toBeTruthy()
  expect(undefined).toBeFalsy()
})

test('number',() => {
  expect(4).toBeGreaterThan(3)
  expect(3).toBeLessThan(4)
})

test('test object', () => {
  // toBe相当于全等
  // expect({a: 1}).toBe({a: 1})

  // toEqual相当于浅比较
  // true
  expect({a: 1}).toEqual({a: 1})
})