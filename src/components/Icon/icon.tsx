import React, { FC, memo } from 'react';
import cx from 'classnames';
import { FontAwesomeIcon, FontAwesomeIconProps } from '@fortawesome/react-fontawesome';

export type ThemeProps = 'primary' | 'secondary' | 'success' | 'info' | 'warning' | 'danger' | 'light' | 'dark'

export interface IconProps extends FontAwesomeIconProps {
  theme?: ThemeProps;
}

const Icon: FC<IconProps> = memo((props) => {
  const { theme, className, ...rest } = props
  const clazz = cx('viking-icon', className, {
    // 当传入了theme属性的时候,就添加icon-${theme}类名
    [`icon-${theme}`]: theme
  })
  return (
    <FontAwesomeIcon
      className={clazz}
      {...rest}  
    />
  )
});

export default Icon;