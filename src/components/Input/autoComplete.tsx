import React, { FC, useState, ChangeEvent } from 'react';
import Input, { InputProps } from '../Input/input'

export interface AutoCompleteProps extends Omit<InputProps, 'onSelect'> {
  fetchSuggestions: (keyword: string) => string[]
  onSelect?: (item: string) => void;
}


export const AutoComplete: FC<AutoCompleteProps> = (props) => {
  const {
    fetchSuggestions,
    onSelect,
    value,
    ...restProps
  } = props

  const [inputValue, setInputValue] = useState(value);
  const [suggestions, setSuggestions] = useState<string[]>([]);


  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    const v = e.target.value.trim()
    setInputValue(v)
    if (v) {
      const results = fetchSuggestions(v)
      setSuggestions(results)
    } else {
      setSuggestions([])
    }
  }

  const handleSelect = (item: string) => {
    setInputValue(item)
    setSuggestions([])
    onSelect?.(item)
  }

  const generateDropdown = () => {
    return (
      <ul>
        {suggestions.map((item, index) => {
          return (
            <li key={index} onClick={() => handleSelect(item)}>{item}</li>
          )
        })}
      </ul>
    )
  }

  return (
    <div className='viking-auto-complete'>
      <Input
        value={value}
        onChange={handleChange}
        {...restProps}
      />
      {
        suggestions.length > 0 && generateDropdown()
      }
    </div>
  )

}
