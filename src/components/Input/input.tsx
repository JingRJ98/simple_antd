import React, { ChangeEvent, FC, InputHTMLAttributes, ReactElement } from 'react';
import cx from 'classnames'
import { IconProp } from '@fortawesome/fontawesome-svg-core';
import Icon from '../Icon/icon'

type InputSize = 'lg' | 'sm'
export interface InputProps extends Omit<InputHTMLAttributes<HTMLElement>, 'size'> {
  disabled?: boolean;
  size?: InputSize;
  icon?: IconProp;
  prepend?: string | ReactElement;
  append?: string | ReactElement;
  onChange?: (e: ChangeEvent<HTMLInputElement>) => void;
}
const Input: FC<InputProps> = (props) => {
  // 取出各种属性
  const {
    disabled,
    size,
    icon,
    prepend,
    append,
    style,
    ...rest
  } = props
  // 根据属性计算不同样式
  const cnames = cx('viking-input-wrapper', {
    [`input-size-${size}`]: size,
    'is-disabled': disabled,
    'input-group': prepend || append,
    'input-group-append': !!append,
    'input-group-prepend': !!prepend
  })

  const fixControlledValue = (value: any) => {
    if (typeof value === 'undefined' || value === null) {
      return ''
    }
    return value
  }
  if ('value' in props) {
    delete rest.defaultValue
    rest.value = fixControlledValue(props.value)
  }
  return (
    <div className={cnames} style={style}>
      {prepend && (
        <div className="viking-input-group-prepand">{prepend}</div>
      )}
      {icon && (
        <div className="icon-wrapper"><Icon icon={icon} title={`title-${icon}`}/></div>
      )}
      <input 
        className='viking-input-inner'
        disabled={disabled}
        {...rest}
      />
       {append && (
        <div className="viking-input-group-append">{append}</div>
      )}
    </div>
  )
};

export default Input;