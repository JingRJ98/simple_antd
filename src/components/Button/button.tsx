import React, { FC } from 'react';
import cn from 'classnames';

export enum ButtonSize {
  Large = 'lg',
  Small = 'sm',
}

export enum ButtonType {
  Primary = 'primary',
  Default = 'default',
  Danger = 'danger',
  Link = 'link',
}

interface BaseButtonProps {
  className?: string;
  disabled?: boolean;
  size?: ButtonSize;
  btnType?: ButtonType;
  children?: React.ReactNode;
  href?: string;
}

// 获取原生的button和a链接的属性(例如原生的onClick等)
type NativeButtonType = BaseButtonProps & React.ButtonHTMLAttributes<HTMLElement>
type AnchorButtonType = BaseButtonProps & React.AnchorHTMLAttributes<HTMLElement>
// 属性设置为可选
export type ButtonProps = Partial<NativeButtonType & AnchorButtonType>

const Button: FC<ButtonProps> = (props) => {
  const {
    btnType,
    className,//用户自己添加的className (这其实是原生上的一个属性)
    disabled,
    size,
    children,
    href,
    ...restProps
  } = props;
  const classes = cn('btn', className, {
    [`btn-${btnType}`]: btnType,
    [`btn-${size}`]: size,
    'disabled': (btnType === ButtonType.Link) && disabled,// 要单独为link设置disabled样式
  });

  if (btnType === ButtonType.Link && href) {
    return (
      <a
        className={classes}
        href={href}
        {...restProps}
      >
        {children}
      </a>
    )
  }
  return (
    <button
      className={classes}
      disabled={disabled}
      {...restProps}
    >
      {children}
    </button>
  )
}
Button.defaultProps = {
  disabled: false,
  btnType: ButtonType.Default,
}

export default Button;
