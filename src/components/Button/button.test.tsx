import React from 'react'
import { render, fireEvent } from '@testing-library/react';
import Button, { ButtonProps, ButtonSize, ButtonType } from './button';
const defaultProps = {
  onClick: jest.fn()
}

const testProps: ButtonProps = {
  btnType: ButtonType.Primary,
  size: ButtonSize.Large,
  className: 'klass'
}

const disabledProps: ButtonProps = {
  disabled: true,
  onClick: jest.fn(),
}

describe(`test Button component`, () => {
  it('should render the correct default button', () => {
    /** example */
    // // destructuring directly from render
    // const { rerender } = render(<Foo />);
    // // saving render result in view
    // const view = render(<Foo />);

    // // destructuring from method wrapping render
    // const setUp = () => render(<Foo />);
    // const { rerender } = setUp();

    // // saving method wrapping render result in view
    // const setUp = () => render(<Foo />);
    // const view = setUp();

    const view = render(<Button {...defaultProps}>nice</Button>)
    // eslint-disable-next-line testing-library/prefer-screen-queries
    const ele = view.getByText('nice') as HTMLButtonElement
    expect(ele).toBeInTheDocument()
    expect(ele.tagName).toEqual('BUTTON')
    expect(ele).toHaveClass('btn btn-default')
    expect(ele.disabled).toBeFalsy()
    fireEvent.click(ele)
    expect(defaultProps.onClick).toHaveBeenCalled()
  })

  it('should render the correct component based on different props', () => {
    const view = render(<Button {...testProps}>nice</Button>)
    // eslint-disable-next-line testing-library/prefer-screen-queries
    const ele = view.getByText('nice')
    expect(ele).toBeInTheDocument()
    expect(ele).toHaveClass('btn-primary btn-lg klass')
  })

  it('should render a link when btnType equals link and href is provided', () => {
    const view = render(<Button btnType={ButtonType.Link} href='www.baidu.com'>Link</Button>)
    // eslint-disable-next-line testing-library/prefer-screen-queries
    const ele = view.getByText('Link')
    expect(ele).toBeInTheDocument()
    expect(ele.tagName).toEqual('A')
    expect(ele).toHaveClass('btn-link btn')
  })

  it('should render disabled button when disabled set to true', () => {
    const view = render(<Button {...disabledProps}>Disabled</Button>)
    // eslint-disable-next-line testing-library/prefer-screen-queries
    const ele = view.getByText('Disabled') as HTMLButtonElement
    expect(ele).toBeInTheDocument()
    expect(ele.disabled).toBeTruthy()
    fireEvent.click(ele)
    expect(disabledProps.onClick).not.toHaveBeenCalled()
  })
})