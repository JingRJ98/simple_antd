import React, { FC } from 'react'
import cn from 'classnames';

export enum AlertPattern {
  Default = 'default',
  Special = 'special',
}

export enum AlertType {
  Success = 'success',
  Default = 'default',
  Danger = 'danger',
  Warning = 'warning',
}

interface BaseAlertProps {
  className?: string;
  pattern?: AlertPattern;
  alertType?: AlertType;
  isShowArrow?: boolean;
  message?: string;
  title?: string;
}

const Alert: FC<BaseAlertProps> = (props) => {
  const {
    pattern,
    alertType,
    isShowArrow,
    className,
    message,
    title,
  } = props;
  const clazz = cn('alert', className, {
    [`alert-${alertType}`]: alertType,
    [`alert-${pattern}`]: pattern,
    [`alert-arrow`]: isShowArrow
  })
  return (
    <div className={clazz} >
      <span>{message}</span>
    </div>
  )
}
Alert.defaultProps = {
  pattern: AlertPattern.Default,
  alertType: AlertType.Default,
}

export default Alert