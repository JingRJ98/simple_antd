import React, { ChangeEvent, FC, useRef } from 'react'
import axios from 'axios'

import Button, { ButtonType } from '../Button/button'

export interface UploadProps {
  /** 发送的接口 */
  action: string;
  beforeUpload?: (file: File) => boolean | Promise<File>
  onProgress?: (percentage: number, file: File) => void;
  onSuccess?: (data: any, file: File) => void;
  onError?: (err: any, file: File) => void;
  onChange?: (file: File) => void;
}
const Upload: FC<UploadProps> = (props) => {
  const {
    action,
    beforeUpload,
    onProgress,
    onSuccess,
    onError,
    onChange,
  } = props

  const fileInputRef = useRef<HTMLInputElement>(null)

  const handleClick = () => {
    if (fileInputRef.current) {
      fileInputRef.current.click()
    }
  }

  const uploadFiles = (files: FileList) => {
    let postFiles = Array.from(files)
    postFiles.forEach(file => {
      if (!beforeUpload) {
        post(file)
      } else {
        const res = beforeUpload?.(file)
        if (res && res instanceof Promise) {
          res.then(processFile => {
            post(processFile)
          })
        } else if (res !== false) {
          post(file)
        }
      }
    })
  }

  const post = (file: File) => {
    const formData = new FormData()
    formData.append(file.name, file)
    axios.post(action, formData, {
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      onUploadProgress: (e) => {
        let percentage = Math.round((e.loaded * 100) / (e?.total ?? 100)) || 0
        if (percentage < 100) {
          onProgress?.(percentage, file)
        }
      }
    }).then(resp => {
      console.log(resp)
      onSuccess?.(resp.data, file)
      onChange?.(file)
    }).catch(e => {
      console.log(e)
      onError?.(e, file)
      onChange?.(file)
    })
  }
  const handleFileChange = (e: ChangeEvent<HTMLInputElement>) => {
    const files = e.target?.files
    if (!files) {
      return
    }
    uploadFiles(files)
    if (fileInputRef.current) {
      fileInputRef.current.value = ''
    }
  }

  return (
    <div
      className='viking-upload-component'
    >
      <Button
        btnType={ButtonType.Primary}
        onClick={handleClick}
      >Upload File</Button>
      <input
        className='viking-file-input'
        style={{ display: 'none' }}
        ref={fileInputRef}
        onChange={handleFileChange}
        type='file'
      ></input>
    </div>
  )
}

export default Upload