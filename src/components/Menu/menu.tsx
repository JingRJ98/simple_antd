import React, { CSSProperties, FC, ReactNode, createContext, useState } from 'react'
import cx from 'classnames';
import { MenuItemProps } from './menuItem'

type MenuMode = 'vertical' | 'horizontal'
type IOnSelect = (selectedIndex: string) => void;

export interface MenuProps {
  defaultIndex?: string;
  className?: string;
  mode?: MenuMode;
  style?: CSSProperties;
  onSelect?: IOnSelect;
  defaultOpenSubMenus?: string[];
  children?: ReactNode;
}

interface IMenuContext {
  index: string;
  onSelect?: IOnSelect;
  mode?: MenuMode;
  defaultOpenSubMenus?: string[];
}

// 默认选中第一个
export const MenuContext = createContext<IMenuContext>({ index: '0' })

const Menu: FC<MenuProps> = (props) => {
  const {
    defaultIndex,
    className,
    mode,
    style,
    onSelect,
    defaultOpenSubMenus,
    children,
  } = props
  const [curActive, setCurActive] = useState(defaultIndex)

  const handleClick = (index: string) => {
    setCurActive(index)
    onSelect?.(index)
  }

  const passedContext: IMenuContext = {
    index: curActive || '0',
    onSelect: handleClick,
    mode,
    defaultOpenSubMenus
  }

  const classes = cx('rj-menu', className, {
    'menu-vertical': mode === 'vertical',
    'menu-horizontal': mode === 'horizontal',
  })

  const renderChildren = () => {
    return React.Children.map(children, (child, i) => {
      const childElement = child as React.FunctionComponentElement<MenuItemProps>
      const {displayName} = childElement.type
      if(displayName === 'MenuItem' || displayName === 'SubMenu') {
        // 创建一个新的组件 自动给它添加index, index用来判断active的是哪个
        return React.cloneElement(childElement, {
          index: `${i}`
        })
      }else{
        // 如果children里面有一个不是MenuItem的组件, 就忽略 且提示警告
        console.error('Warning: Menu has a child which is not a <MenuItem/> ')
      }
    })
  }
  return (
    <ul
      className={classes}
      style={style}
      data-testid='test-menu'
    >
      <MenuContext.Provider value={passedContext}>
        {renderChildren()}
      </MenuContext.Provider>
    </ul>
  )
}
Menu.defaultProps = {
  defaultIndex: '0',
  mode: 'horizontal',
  defaultOpenSubMenus: []
}

export default Menu