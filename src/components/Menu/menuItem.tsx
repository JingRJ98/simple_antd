import React, { CSSProperties, FC, ReactNode, useContext } from 'react'
import cx from 'classnames'
import { MenuContext } from './menu'

export interface MenuItemProps {
  index?: string;
  disabled?: boolean;
  className?: string;
  style?: CSSProperties;
  children?: ReactNode;
}

const MenuItem: FC<MenuItemProps> = (props) => {
  const {
    index,
    disabled,
    className,
    style,
    children
  } = props
  const context = useContext(MenuContext)
  const classes = cx('menu-item', className, {
    'is-disabled': disabled,
    'is-actived': index === context.index,
  })

  const handleClick = () => {
    if (!disabled && (typeof index === 'string')) {
      context.onSelect?.(index || '0')
    }
  }
  return (
    <li
      className={classes}
      style={style}
      onClick={handleClick}
    >
      {children}
    </li>
  )
}

MenuItem.displayName = 'MenuItem'
export default MenuItem