import React, { FC, FunctionComponentElement, ReactNode, useContext, useRef, useState } from 'react';
import cx from 'classnames';
import { CSSTransition } from 'react-transition-group';
import { MenuContext } from './menu';
import { MenuItemProps } from './menuItem'
import Icon from '../Icon/icon';
import Transition from '../Transition/transition';

export interface SubMenuProps {
  index?: string;
  title: string;
  className?: string;
  children?: ReactNode;
}

const SubMenu: FC<SubMenuProps> = ({ index, title, className, children }) => {
  const context = useContext(MenuContext)
  const openSubMenus = context.defaultOpenSubMenus as string[]
  const [menuOpen, setMenuOpen] = useState(
    (index && context.mode === 'vertical') ? openSubMenus.includes(index) : false
  )

  const nodeRef = useRef(null);

  const classes = cx('menu-item', 'submenu-item', className, {
    'is-active': context.index === index,
    'is-opened': menuOpen,
    'is-vertical': context.mode === 'vertical'
  })

  const handleClick = (e: React.MouseEvent) => {
    e.preventDefault()
    setMenuOpen(!menuOpen)
  }
  let timer: any
  const handleMouse = (e: React.MouseEvent, toggle: boolean) => {
    clearTimeout(timer)
    e.preventDefault()
    timer = setTimeout(() => {
      setMenuOpen(toggle)
    }, 300)
  }

  // 垂直模式下的subMenu点击展开
  const clickEvents = context.mode === 'vertical' ? {
    onClick: handleClick
  } : {}
  const mouseEvents = context.mode !== 'vertical' ? {
    onMouseEnter: (e: React.MouseEvent) => handleMouse(e, true),
    onMouseLeave: (e: React.MouseEvent) => handleMouse(e, false),
  } : {}

  const renderChildren = () => {
    const childrenComponent = React.Children.map(children, (child, i) => {
      const childEle = child as FunctionComponentElement<MenuItemProps>
      if (childEle.type.displayName === 'MenuItem') {
        return React.cloneElement(childEle, {
          index: `${index}-${i}`
        })
      } else {
        console.error('warning: Submenu has a child which is not a MenuItem component')
      }
    })
    return (
      <Transition
        in={menuOpen}
        timeout={300}
        nodeRef={nodeRef}
        animation="zoom-in-top"
      >
        <ul 
          className={cx('viking-submenu', {
            'menu-opened': menuOpen
          })}
          ref={nodeRef}
        >
          {childrenComponent}
        </ul>
      </Transition>
    )
  }
  return (
    <li key={index} className={classes} {...mouseEvents}>
      <div className='submenu-title' {...clickEvents}>
        {title}
        <Icon icon='angle-down' className="arrow-icon" />
      </div>
      {renderChildren()}
    </li>
  )
}

SubMenu.displayName = 'SubMenu'

export default SubMenu