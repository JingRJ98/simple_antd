import React from 'react'
import { fireEvent, render, RenderResult, cleanup, waitFor } from '@testing-library/react'

import Menu, { MenuProps } from './menu'
import MenuItem from './menuItem'
import SubMenu from './subMenu'

const testProps: MenuProps = {
  defaultIndex: '0',
  onSelect: jest.fn(),
  className: 'test',
}

const testVerProps: MenuProps = {
  defaultIndex: '0',
  mode: 'vertical',
  onSelect: jest.fn(),
}

const generateMenu = (props: MenuProps) => {
  return (
    <Menu
      {...props}
    >
      <MenuItem index={'0'}>
        active
      </MenuItem>
      <MenuItem index={'1'} disabled>
        disabled
      </MenuItem>
      <MenuItem index={'2'}>
        xyz
      </MenuItem>
      <SubMenu title='dropdown'>
        <MenuItem>
          drop1 
        </MenuItem>
      </SubMenu>
    </Menu>
  )
}

const createStyleFile = () => {
  const cssFile: string = `
    .viking-submenu {
      display: none;
    }
    .viking-submenu.menu-opened {
      display: block;
    }
  `
  const style = document.createElement('style')
  style.type = 'text/css'
  style.innerHTML = cssFile
  return style
}

let wrapper: RenderResult, menuElement: HTMLElement, activeElement: HTMLElement, disabledElement: HTMLElement

describe('test Menu and MenuItem componetn', () => {
  beforeEach(() => {
    // eslint-disable-next-line testing-library/no-render-in-setup
    wrapper = render(generateMenu(testProps))
    // eslint-disable-next-line testing-library/prefer-screen-queries
    menuElement = wrapper.getByTestId('test-menu')
    // eslint-disable-next-line testing-library/prefer-screen-queries
    activeElement = wrapper.getByText('active')
    // eslint-disable-next-line testing-library/prefer-screen-queries
    disabledElement = wrapper.getByText('disabled')
  })
  it('should render correct Menu and MenuItem based on default props', () => {
    expect(menuElement).toBeInTheDocument()
    expect(menuElement).toHaveClass('rj-menu test')
    // eslint-disable-next-line testing-library/no-node-access
    expect(menuElement.querySelectorAll(':scope > li').length).toEqual(4)
    expect(activeElement).toHaveClass('menu-item is-actived')
    expect(disabledElement).toHaveClass('menu-item is-disabled')
  })

  it('click items should change active and call the right callback', () => {
    // eslint-disable-next-line testing-library/prefer-screen-queries
    const thirdItem = wrapper.getByText('xyz')
    fireEvent.click(thirdItem)
    expect(thirdItem).toHaveClass('is-actived')
    // 点击第三个之后原来的activeItem不在有这个类名
    expect(activeElement).not.toHaveClass('is-actived')
    expect(testProps.onSelect).toHaveBeenCalledWith('2')
    fireEvent.click(disabledElement)
    expect(disabledElement).not.toHaveClass('is-actived')
    expect(testProps.onSelect).not.toHaveBeenCalledWith('1')
  })

  it('should render vertical mode when mode is set to vertical', () => {
    // 清除之前的节点
    cleanup()
    // eslint-disable-next-line testing-library/render-result-naming-convention
    const wrapper = render(generateMenu(testVerProps))
    // eslint-disable-next-line testing-library/prefer-screen-queries
    const menuElement = wrapper.getByTestId('test-menu')
    expect(menuElement).toHaveClass('menu-vertical')
  })

  it('should show dropdown items when hover on submenu', async () => {
    cleanup()
    // eslint-disable-next-line testing-library/render-result-naming-convention
    const wrapper = render(generateMenu(testProps))
    // eslint-disable-next-line testing-library/no-container
    wrapper.container.append(createStyleFile())
    // eslint-disable-next-line testing-library/prefer-screen-queries
    expect(wrapper.queryByText('drop1')).not.toBeVisible()
    // eslint-disable-next-line testing-library/prefer-screen-queries
    const dropDownEle = wrapper.getByText('dropdown')
    expect(dropDownEle).toBeVisible()
    fireEvent.mouseEnter(dropDownEle)
    await waitFor(() => {
      // eslint-disable-next-line testing-library/prefer-screen-queries
      expect(wrapper.queryByText('drop1')).toBeVisible()
    })
    fireEvent.mouseLeave(dropDownEle)
    await waitFor(() => {
      // eslint-disable-next-line testing-library/prefer-screen-queries
      expect(wrapper.queryByText('drop1')).not.toBeVisible()
    })
  })

  it('should show dropdown items when click on submenu', () => {
    // 清除之前的dom节点
    cleanup();
    // eslint-disable-next-line testing-library/render-result-naming-convention
    const wrapper = render(generateMenu(testVerProps))
    // 添加下拉菜单的样式文件
    // eslint-disable-next-line testing-library/no-container
    wrapper.container.append(createStyleFile())
    // eslint-disable-next-line testing-library/prefer-screen-queries
    expect(wrapper.queryByText('drop1')).not.toBeVisible()

    // eslint-disable-next-line testing-library/prefer-screen-queries
    const dropDownEle = wrapper.getByText('dropdown')
    fireEvent.click(dropDownEle)
    // eslint-disable-next-line testing-library/prefer-screen-queries
    expect(wrapper.queryByText('drop1')).toBeVisible()
    // eslint-disable-next-line testing-library/prefer-screen-queries
    fireEvent.click(wrapper.getByText('drop1'))
    expect(testVerProps.onSelect).toHaveBeenCalledWith('3-0')
  })
})