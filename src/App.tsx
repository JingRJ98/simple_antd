import React from 'react';
import { library } from '@fortawesome/fontawesome-svg-core';
// 引入所有图标
import { fas } from '@fortawesome/free-solid-svg-icons';

import Alert, { AlertPattern, AlertType } from './components/Alert/alert';
import Button, { ButtonType, ButtonSize } from './components/Button/button';
import Icon from './components/Icon/icon';
import Menu from './components/Menu/menu';
import MenuItem from './components/Menu/menuItem';
import SubMenu from './components/Menu/subMenu';
import Upload from './components/Upload/upload';

library.add(fas);

function App() {
  return (
    <div className="App">
      {/* <Button onClick={e => { console.log() }} >hello world</Button>
      <Button btnType={ButtonType.Primary}>primary</Button>
      <Button btnType={ButtonType.Primary} size={ButtonSize.Large}>primaryLarge</Button>
      <Button btnType={ButtonType.Danger} size={ButtonSize.Small}>dangerSmall </Button>
      <Button btnType={ButtonType.Danger}>danger</Button>
      <Button btnType={ButtonType.Link} href={`https://www.baidu.com/`}>Link</Button>
      <Button disabled>disabled</Button> */}
      {/* <Alert 
        message='Hello World'
      /> */}

      {/* <Menu
        onSelect={(index) => console.log('index :>> ', index)}
        mode='horizontal'
        defaultOpenSubMenus={['2']}
      >
        <MenuItem>
          apple
        </MenuItem>
        <MenuItem disabled>
          orange
        </MenuItem>
        <SubMenu title='dropdown'>
          <MenuItem>
            dropdown1
          </MenuItem>
          <MenuItem>
            dropdown2
          </MenuItem>
        </SubMenu>
        <MenuItem>
          banana
        </MenuItem>
      </Menu>
      
      <Icon icon='coffee' size='10x' theme='danger'></Icon>
      <Icon icon='arrow-down' size='10x' theme='primary'></Icon>
      <Icon icon='arrow-up' size='10x' theme='warning'></Icon> */}

      <Upload action='https://jsonplaceholder.typicode.com/posts/'></Upload>
    </div>
  );
}

export default App;
